﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GerenciadorComponente : Photon.MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (!photonView.isMine)
        {
            Destroy(GetComponentInChildren<FlareLayer>());
            Destroy(GetComponentInChildren<Camera>());
            Destroy(GetComponentInChildren<AudioListener>());
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<Movement>());
            Destroy(GetComponent<Movement>());
            Destroy(GetComponent<PLShooting>());
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
