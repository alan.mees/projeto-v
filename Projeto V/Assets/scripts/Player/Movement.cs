﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    bool GameOver = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!GameOver)
        {
            MouseRotation();
            WalkFunction();
        }

    }

    void WalkFunction()
    {       
        //var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

       // transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }

    private void MouseRotation()
    {
        float mouseInput = Input.GetAxis("Mouse X") * Time.deltaTime * 450.0f;
        Vector3 lookhere = new Vector3(0, mouseInput, 0);
        transform.Rotate(lookhere);
    }
}
