﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState { NONE, GAUGE, HIDE, RUN, FEAR, CUTSCENE };

public class GameManager : MonoBehaviour {
        
    //criar apenas um GM e referencia do player
    public static GameManager Instance = null;    
    public GameObject PlayerRef;

    //fim de jogo
    public bool IsGameOver = false;
    //modos de jogo
    public GameState GMStates = GameState.NONE;
    
    void Awake()
    {       
        if (Instance == null)            
            Instance = this;
        
        else if (Instance != this)            
            Destroy(gameObject);       
    }

    public void RandomPickPlayerToBeMonster()
    {
        //passar pela lista de jogadores ingame e escolher randomicamente somente UM para começar sendo o monstro
        //dentro de cada player vai ter um script do ti PLCollision e dentro tera a função SetIsHuman(bool IsIt) é só mandar para o player escolhido
        //e enviar um true para quem for o mosntro, dentro desta funçao a gente faz set de mesh, força e tudo mais que o monstro
        //tera de diferente dos outros player


    }

    public void ChangeGameState(int RefGM)
    {
        switch(RefGM)
        {
            case 0:
                GMStates = GameState.NONE;
                break;
            case 1:
                GMStates = GameState.GAUGE;
                break;
            case 2:
                GMStates = GameState.HIDE;
                break;
            case 3:
                GMStates = GameState.RUN;
                break;
            case 4:
                GMStates = GameState.FEAR;
                break;
            case 5:
                GMStates = GameState.CUTSCENE;
                break;
        }
    }

        // Use this for initialization
    void Start ()
    {
       
    }

	
	// Update is called once per frame
	void Update ()
    {
       
    }

    private void LateUpdate()
    {
        
    }
}
