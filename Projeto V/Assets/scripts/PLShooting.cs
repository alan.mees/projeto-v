﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLShooting : Photon.MonoBehaviour {

    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Fire", 0, 1);
    }

	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Fire();
        }
    }

    void Fire()
    {
        // Create the Bullet from the Bullet Prefab
        //var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        var bullet = PhotonNetwork.Instantiate("Bullet", bulletSpawn.position, bulletSpawn.rotation, 0);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 50;

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }
}
