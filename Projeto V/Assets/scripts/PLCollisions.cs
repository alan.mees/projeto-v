﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLCollisions : MonoBehaviour {


    public bool isHuman = true;
    public int currentHealth = 100;
    public GameObject MonsterReference;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SetIsHuman(bool IsIt)
    {
        isHuman = IsIt;
        GetComponent<Renderer>().material = MonsterReference.GetComponent<Renderer>().material;

        //adicionar outros atributos que o monstro terá de diferente dos playes normais
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Monster")
        {
            isHuman = false;
            GetComponent<Renderer>().material = other.gameObject.GetComponent<Renderer>().material;
            Debug.Log("IsHuman: " + isHuman);
        }

        if (other.gameObject.tag == "Friend")
        {
            GetComponent<Renderer>().material = other.gameObject.GetComponent<Renderer>().material;
            Debug.Log("Friend Collected");
        }
    }
}
