﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conexao : Photon.MonoBehaviour {

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("1.0");
        
    }
    

    public void Connectar()
    {
        PhotonNetwork.ConnectUsingSettings("1.0");
    }
    
    void OnConnectedToMaster()
    {
        
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, MaxPlayers = 10 };
        PhotonNetwork.JoinOrCreateRoom("Sala01", roomOptions, TypedLobby.Default);
    }
    void OnJoinedRoom()
    {
        if (PhotonNetwork.isMasterClient)
            PhotonNetwork.Instantiate("Player", new Vector3(0f, 2.53f, 0f), Quaternion.identity, 0);
        else
            PhotonNetwork.Instantiate("Monster", new Vector3(0f, 2.53f, 24.1f), Quaternion.identity, 0);
    }
}
