﻿using UnityEngine;

namespace Assets.scripts.Conexao
{
    public class Sincronizacao : Photon.MonoBehaviour
    {
        public Transform target;

        // Use this for initialization
        void Start()
        {
            photonView.ObservedComponents.Add(gameObject.GetComponent<PhotonTransformView>());
        }
        
        void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.isWriting)
            {
                stream.SendNext(transform.position);
                stream.SendNext(transform.rotation);
            }
            else
            {
                transform.position = (Vector3)stream.ReceiveNext();
                transform.rotation = (Quaternion)stream.ReceiveNext();
            }
        }
    }
}
