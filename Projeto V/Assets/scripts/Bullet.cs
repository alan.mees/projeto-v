﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Photon.MonoBehaviour {

    public float forceToApply = 6f;

    // Use this for initialization
    void Start () {
        if (!photonView.isMine)
        {
            Destroy(GetComponent<Rigidbody>());
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("Touched Monster");

        if(other.gameObject.tag == "Monster")
        {
            // Calculate Angle Between the collision point and the player
            Vector3 dir = other.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = dir.normalized;
            // And finally we add force in the direction of dir and multiply it by force. 
            // This will push back the player
            other.gameObject.GetComponent<Rigidbody>().AddForce(dir * forceToApply, ForceMode.Impulse);
            
        }

        PhotonNetwork.Destroy(photonView);
    }
}
